package com.example.coronavirusnews;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

public class GetNews extends AsyncTask<String, String, String> {

    private ProgressDialog pd;
    private ArrayList<News> newsArrayList ;
    private Context ctx;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;

    public GetNews(Context ctx, RecyclerView main_RecyclerView) {
        this.mRecyclerView = main_RecyclerView;
        this.ctx = ctx;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        newsArrayList = new ArrayList<News>();
        pd = new ProgressDialog(ctx);
        pd.setMessage("A obter dados, aguarde por favor");
        pd.setCancelable(false);
        pd.show();
    }

    @Override
    protected String doInBackground(String... params) {
        URLConnection urlConn = null;
        BufferedReader bufferedReader = null;

        try {
            //transforma url em string
            URL url = new URL(params[0]);
            urlConn = url.openConnection();
            bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

            StringBuffer stringBuffer = new StringBuffer();
            String line;

            while ( (line = bufferedReader.readLine())!= null){
                stringBuffer.append(line);
            }
            return stringBuffer.toString();


        }catch (Exception ex){
            Log.e("TAG", "JsonTask: " , ex);
            return null;
        }finally {
            if(bufferedReader != null){
                try {
                    bufferedReader.close();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        }
    }


    @Override
    protected void onPostExecute(String response) {
        //no fim de executar obtem a string
        if(response != null){
            try {

              //  JSONObject jsonObject= new JSONObject(response).getJSONObject("response");
                JSONObject jsonObject= new JSONObject(response);
                JSONArray jsonArrayResults = jsonObject.getJSONArray("articles");


                for(int i=0; i<jsonArrayResults.length();i++){
                    String webTitle = jsonArrayResults.getJSONObject(i).getString("title");
                    String webUrl = jsonArrayResults.getJSONObject(i).getString("url");
                    String fields_firstPublicationDate = jsonArrayResults.getJSONObject(i).getString("publishedAt");
                    String thumbnail = jsonArrayResults.getJSONObject(i).getString("urlToImage");
                    String content = jsonArrayResults.getJSONObject(i).getString("content");
                    News newsVar = new News(webTitle,webUrl,thumbnail,fields_firstPublicationDate, content);

                    /*String webTitle = jsonArrayResults.getJSONObject(i).getString("webTitle");
                    String webUrl = jsonArrayResults.getJSONObject(i).getString("webUrl");
                    String fields_firstPublicationDate =  jsonArrayResults.getJSONObject(i).getJSONObject("fields").getString("firstPublicationDate");
                    String thumbnail =  jsonArrayResults.getJSONObject(i).getJSONObject("fields").getString("thumbnail");
                    News newsVar = new News(webTitle,webUrl,thumbnail,fields_firstPublicationDate);*/
                    newsArrayList.add(newsVar);
                }


               mRecyclerView.setHasFixedSize(true);
                layoutManager = new LinearLayoutManager(ctx);
                mRecyclerView.setLayoutManager(layoutManager);
                mAdapter = new RecyclerAdapter(ctx, newsArrayList);
                mRecyclerView.setAdapter(mAdapter);

            } catch (JSONException e) {
                Log.e("App", "Failure", e);
                e.printStackTrace();
            }
        }else{
           // tv.append("resposta vazia");
        }

        if(pd.isShowing()){
            pd.dismiss();
        }
    }

}
