package com.example.coronavirusnews;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.Serializable;


public class NewFragment extends Fragment {


    private TextView tvTitle, tvContent;
    private ImageView imgViewNew;
    private Button btnView;

    public NewFragment() {
        // Required empty public constructor
    }

    News single_new;

    public static NewFragment newInstance(News single_new){
        NewFragment fragment = new NewFragment();
        Bundle args = new Bundle();
        args.putSerializable("single_new", single_new);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null){
            single_new = (News)getArguments().getSerializable("single_new");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_new, container, false);

        tvTitle = v.findViewById(R.id.tvTitle);
        tvContent = v.findViewById(R.id.tvContent);
        imgViewNew = v.findViewById(R.id.imageView);
        btnView = v.findViewById(R.id.button);


        tvTitle.setText(single_new.getWebTitle());
        tvContent.setText(single_new.getContent());

        //carrega imagem do url
        new DownloadImageTask(imgViewNew).execute(single_new.getFields_thumbnail());

        btnView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse(single_new.getWebUrl()));
                getContext().startActivity(intent);
            }
        });

        return v;
    }
}
