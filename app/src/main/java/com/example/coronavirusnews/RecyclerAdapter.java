package com.example.coronavirusnews;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final int TYPE = 1;
    private final Context context;
    private final ArrayList<News> listRecyclerItem;


    public RecyclerAdapter(Context context, ArrayList<News> listRecyclerItem) {
        this.context = context;
        this.listRecyclerItem = listRecyclerItem;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        switch (viewType){
            case TYPE:

            default:
                View layoutView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.news_row, null);
                return new ViewHolder((layoutView));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        int viewType = getItemViewType(position);

        switch (viewType) {
            case  TYPE:
            default:
                ViewHolder itemViewHolder = (ViewHolder) holder;

                final News news = (News) listRecyclerItem.get(position);

                //set titutlo
                itemViewHolder.tvWebTitle.setText(news.getWebTitle());

                //carrega imagem do url
                new DownloadImageTask( itemViewHolder.imgFields_thumbnail).execute(news.getFields_thumbnail());

                //listner cada btn
            /*    itemViewHolder.btnWebUrl.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent();
                        intent.setAction(Intent.ACTION_VIEW);
                        intent.addCategory(Intent.CATEGORY_BROWSABLE);
                        intent.setData(Uri.parse(news.getWebUrl()));
                        context.startActivity(intent);
                    }
                });*/

                //Implementacao do botao para entra no perfil do doente
                itemViewHolder.btnWebUrl.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        News single_new = (News) listRecyclerItem.get(position);
                        NewFragment newAppFragment = NewFragment.newInstance(single_new);

                        FragmentTransaction transaction = ((MainActivity) v.getContext()).getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.fragment_container, newAppFragment);
                        transaction.addToBackStack(null);
                        transaction.commit();
                    }

                });
        }
    }

    @Override
    public int getItemCount() {
        return listRecyclerItem.size();
    }

    public class ViewHolder  extends RecyclerView.ViewHolder{

        private TextView tvWebTitle;
        private ImageView imgFields_thumbnail;
        private TextView btnWebUrl;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvWebTitle = (TextView) itemView.findViewById(R.id.webTitle);
            imgFields_thumbnail =  itemView.findViewById(R.id.fields_thumbnail);
            btnWebUrl = (TextView) itemView.findViewById(R.id.webUrl);
        }

    }


}
