package com.example.coronavirusnews;


//https://content.guardianapis.com/search?q=coronavirus&format=json&show-fields=bodyText,thumbnail,firstPublicationDate&order-by=newest&api-key=d797d030-b8f6-4452-bf49-efb902159458

import java.io.Serializable;

public class News implements Serializable {
    private  String webTitle;
    private  String webUrl;
    private  String fields_thumbnail;
    private  String fields_firstPublicationDate;
    private String content;

    public News(String webTitle, String webUrl, String fields_thumbnail, String fields_firstPublicationDate, String content) {
        this.webTitle = webTitle;
        this.webUrl = webUrl;
        this.fields_thumbnail = fields_thumbnail;
        this.fields_firstPublicationDate = fields_firstPublicationDate;
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getWebTitle() {
        return webTitle;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public String getFields_thumbnail() {
        return fields_thumbnail;
    }


    public String getFields_firstPublicationDate() {
        return fields_firstPublicationDate;
    }

    public void setWebTitle(String webTitle) {
        this.webTitle = webTitle;
    }

    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    public void setFields_thumbnail(String fields_thumbnail) {
        this.fields_thumbnail = fields_thumbnail;
    }


    public void setFields_firstPublicationDate(String fields_firstPublicationDate) {
        this.fields_firstPublicationDate = fields_firstPublicationDate;
    }
}
