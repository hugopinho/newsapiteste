package com.example.coronavirusnews;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.android.material.navigation.NavigationView;


public class AboutUsFragment extends Fragment {

    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;

    //Criado Listener para a interface
    private OnFragmentButtonClick listener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_about_us, container, false);

        Button load = v.findViewById(R.id.btnHome);
        //registar listner no btn
        load.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onButtonClicked();
            }
        });

        return v;
    }

    //metedo quando o fragmento é carregado na atividade
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if(context instanceof OnFragmentButtonClick){
            listener = (OnFragmentButtonClick) context;
        }else {
            throw new ClassCastException(context.toString() + "lister not implemented");
        }
    }

    //Criação de interface lado fragment
    public interface OnFragmentButtonClick{
        public void onButtonClicked();

    }
}
