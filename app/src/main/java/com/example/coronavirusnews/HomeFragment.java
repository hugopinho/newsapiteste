package com.example.coronavirusnews;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;



public class HomeFragment extends Fragment {
    private RecyclerView mRecyclerView;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_home, container, false);

        mRecyclerView = (RecyclerView) v.findViewById(R.id.my_recycler_view);
       GetNews getNews = new GetNews(this.getContext(),mRecyclerView);
      // getNews.execute("https://content.guardianapis.com/search?q=coronavirus&format=json&show-fields=thumbnail,firstPublicationDate&order-by=newest&api-key=d797d030-b8f6-4452-bf49-efb902159458");
        getNews.execute("https://newsapi.org/v2/top-headlines?country=pt&category=technology&apiKey=f385f2f5537f4ec89520d24b504b1192");
        return v;
    }
}
